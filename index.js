db.fruits.insertMany([
	{
		"name": "Apple",
		"color": "Red",
		"enoughStock": 20,
		"price": 40,
		"supplier_id": 1,
		"fruitsOnSale": true,
		"origin": ["Philippines", "US"]
	},
		{
		"name": "Banana",
		"color": "Yellow",
		"enoughStock": 15,
		"price": 20,
		"supplier_id": 2,
		"fruitsOnSale": true,
		"origin": ["Philippines", "Ecuador"]
	},
		{
		"name": "Kiwi",
		"color": "Green",
		"enoughStock": 25,
		"price": 50,
		"supplier_id": 1,
		"fruitsOnSale": true,
		"origin": ["China", "US"]
	},
		{
		"name": "Mango",
		"color": "Yellow",
		"enoughStock": 10,
		"price": 120,
		"supplier_id": 2,
		"fruitsOnSale": false,
		"origin": ["Philippines", "India"]
	}

	]);

//db.fruits.find({onSale: true}).count()

db.fruits.aggregate([ 
	{ $match: {fruitsOnSale: true} },
	{ $count: "fruitsOnSale"}
       
	]);

db.fruits.aggregate([ 
	{ $match: {enoughStock: {$gte: 20}} },
	{ $count: "enoughStock"}
	]);

db.fruits.aggregate([ 
	{ $match: {onSale: true} },
	{ $group: {_id: "$supplier_id", avg_price: {$avg: "$price"}} },
       
	]);

db.fruits.aggregate([ 
	{ $match: {onSale: true} },
	{ $group: {_id: "$supplier_id", max_price: {$max: "$price"}} },
       
	]);

db.fruits.aggregate([ 
	{ $match: {onSale: true} },
	{ $group: {_id: "$supplier_id", min_price: {$min: "$price"}} },
       
	]);

